# -*- coding: utf-8 -*-

from odoo import models, fields, api
import psutil, shutil, requests
import datetime

class DidomiHitory(models.Model):
    _name = 'didomi.history'

    name = fields.Float()
    cpu_used = fields.Float(string="CPU")
    ram_used = fields.Float(string="RAM")
    disc_used = fields.Float(string="DISC")
    ping_time = fields.Float(string="PING")

    @api.model
    def create_data(self):
        """
            generate all 5 mn a data for didomi model
            retur: True
        """
        vals = {
            'name': datetime.datetime.now().minute,
            'cpu_used': psutil.cpu_percent(),
            'ram_used': psutil.virtual_memory()[2],
            'disc_used': psutil.disk_usage('/').used / (1024.0 ** 3),
            'ping_time': requests.get("http://www.yahoo.fr").elapsed.total_seconds()
        }
        self.create(vals)
        return True